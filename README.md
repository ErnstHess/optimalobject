# OptimalObject
I made this project to help me find the most reliable way of force-rendering PDFs within a website, across the widest range of differet browsers possible (by opening the demo in [BrowserStack](https://www.browserstack.com/)).
Probably useless for anyone but me. Ended up going with [PDFObject library](https://pdfobject.com/).

Project demo: [https://ernsthess.gitlab.io/optimalobject/index.html](https://ernsthess.gitlab.io/optimalobject/index.html "Demo here")